#!/bin/bash
#SBATCH --job-name="next-char"
#SBATCH --time=00:10:00
#SBATCH -D .
#SBATCH --output=LSTM_%J.out
#SBATCH --error=LSTM_%J.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --qos=debug


export NX_ARGS="--summary --smp-workers=48"
./examples/d_textgen_parallel -P -t 6 -B 64 -l 12 -u 100 -r 0.001 -n 128 -m 1 -M "lstm" ./examples/10000_wiki
# Seq (Sequential) run code
export NX_ARGS="--summary --smp-workers=48"
./examples/d_textgen_parallel -t 6 -B 64 -l 12 -u 100 -r 0.001 -n 128 -m 1 -M "lstm" ./examples/10000_wiki


