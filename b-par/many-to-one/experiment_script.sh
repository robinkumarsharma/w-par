#!/bin/bash
#SBATCH --job-name="BiDir_LSTM"
#SBATCH --time=01:59:00
#SBATCH -D .
#SBATCH --output=BiDir_LSTM-%J.out
#SBATCH --error=BiDir_LSTM-%J.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
##SBATCH --gres=gpu:4
#SBATCH --exclusive
##SBATCH --qos=bsc_cs
#SBATCH --qos=debug

SCRIPT=./benchmark_model
in=./data/I_128x100x256.txt
out=./data/O_128x100x256.txt

# B-Par (Bidirectional-Parallel) run code
export NX_ARGS="--summary --smp-workers=48"
$SCRIPT -P -D 2 -n 256 -b 128 -l 6 -t 6 -j 48 -M lstm $in $out

# B-Seq (Bidirectional-Sequential) run code
export NX_ARGS="--summary --smp-workers=48"
$SCRIPT -D 2 -n 256 -b 128 -l 6 -t 6 -j 48 -M lstm $in $out
