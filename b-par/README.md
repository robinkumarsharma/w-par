#SC'2021 Submission

## Getting Started
```sh
# acquire source code
git clone https://gitlab.com/robinkumarsharma/w-par.git
cd W-Par/b-par/many-to-one 

## Speech recognition task (BRNNs many-to-one model)
# B-Par (Bidirectional-Parallel) recurrent neural network run code
./benchmark_model -P -D 2 -n 256 -b 128 -l 6 -t 6 -j 48 -M lstm ./data/I_128x100x256.txt ./data/O_128x100x256.txt
# B-Seq (Bidirectional-Sequential) run code
./benchmark_model -D 2 -n 256 -b 128 -l 6 -t 6 -j 48 -M lstm ./data/I_128x100x256.txt ./data/O_128x100x256.txt

## Next-Char Prediction Task (BRNNs many-to-many model)
cd W-Par/b-par/many-to-many
# B-Par (Bidirectional-Parallel) recurrent neural network run code
 ./parallel_rnn -P -D 2 -f 0.1 -t 6 -B 64 -l 12 -u 100 -r 0.01 -n 128 -m 1 -M "lstm" ./data/10000_wiki
# B-Seq (Bidirectional-Sequential) run code
./parallel_rnn -D 2 -f 0.1 -t 6 -B 64 -l 12 -u 100 -r 0.01 -n 128 -m 1 -M "lstm" ./data/10000_wiki
```

### Parameters
* -P : Parallel implementation
* -D : Bi-Directional design reference
* -b/B : Batch Size
* -t : Number of mini-batches to divide Batch Size to run in parallel
* -l : Number of layers
* -u : Sequnce or Unrolling length
* -r : Learning rate
* -n : Hidden Size/Number of neurons
* -m : Number of Epoch
* -M : model ["lstm"/"gru"]
* -j : #cores reference
* ./data/I_128x100x256.txt : Processed input data for Specch task
* /data/O_128x100x256.txt : Processed output data for Specch task
* 10000\_wiki : A part of the wikipedia dataset

### Sample Results
We use the BRNN (Bi-directional Recurrent neural networks) LSTM varient to perform the speech recognition task and the next character on the set of wikipedia dataset and following are the results for execution time of single batch:

|Task       |Parallel/Sequential|Machine|Device   |Training time [ms] | Validation time [ms]      |Command line |
|:----------|:------------|:-------------|:--------|:--------|:----------|:------------|
| Speech recognition (many-to-one model) | Parallel    |Linux  |48 CPU    | 1068.67  | - |./benchmark_model -P -D 2 -n 256 -b 128 -l 6 -t 6 -j 48 -M lstm ./data/I_128x100x256.txt ./data/O_128x100x256.txt
|  | Sequential    |Linux  |48 CPU    | 2987.56  | - |./benchmark_model -D 2 -n 256 -b 128 -l 6 -t 6 -j 48 -M lstm ./data/I_128x100x256.txt ./data/O_128x100x256.txt
|Next-Char Prediction (many-to-many model)  | Parallel    |Linux  |48 CPU    | 2044.77  | 334.81 |./examples/d\_textgen\_parallel -P -t 6 -B 64 -l 12 -u 100 -r 0.001 -n 128 -m 1 -M "rnn" ./examples/200\_wiki
|  | Sequential    |Linux  |48 CPU    | 5854.10  | 1384.89 |./parallel_rnn -D 2 -f 0.1 -t 6 -B 64 -l 12 -u 100 -r 0.01 -n 128 -m 1 -M lstm ./data/10000_wiki


The above Speech recognition resuls refer to the results in paper, Table-3 configuration 256/256/128/100 . The training time [ms] for B-Par, data parallelism approach is **1068.67** and in B-Seq, data+parallelism approach is **2044.77** . Due to model parallelism, there is speed up of **1.91** considering sequential execution against parallel execution. Sample Results can be found at: wpar/b-par/many-to-one/Sample\_Results .

For Next-Prediction Task, the training time [ms] for B-Par is **2044.77** and in B-Seq run is **5854.10** . Due to model parallelism, there is speed up of **2.86** considering sequential execution against parallel execution. Sample Results can be found at: wpar/b-par/many-to-many/Sample\_Results .

### Requirements
Following are the requirements to execute the above commands:

* Linux x86-64 platform
* GNU C/C++ compiler versions 8.1.0
* [OmpSs-1.0][ompss1.0]
* Sequential Intel MKL library 2019.3


[ompss1.0]: https://pm.bsc.es/ftp/ompss/doc/user-guide/
